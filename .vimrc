set number
set relativenumber

set background=dark

syntax on

set autoindent
set shiftwidth=4
set softtabstop=4
